package com.forty2andpark.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Send
 */
@WebServlet({ "/Send", "/send" })
public class Send extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Send() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String result = "{\"result\":\"error\"}";
		System.out.println("send query:" + request.getQueryString());
		String message = (String)request.getParameter("message");
		if(null != message && !message.isEmpty()) {
			System.out.println("message: " + message);
			HttpSession session = request.getSession(true);
			System.out.println("session: " + session.getId());
			if(session.isNew() || null == session.getAttribute("messages") )
				session.setAttribute("messages", new ArrayList<String>());
			List<String> messages = (List<String>)session.getAttribute("messages");
			System.out.println("messages: " + messages);
			Date ts = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
			String tsStr = sdf.format(ts);
			System.out.println("timstamp: " + tsStr);
			messages.add(tsStr + ": " + message);
			session.setAttribute("messages", messages);
			result = "{\"result\":\"success\"}";
		}
		else {
			result = "{\"result\":\"empty\"}";
		}

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(result);
		out.flush();	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private void addMessage(HttpSession session, String message){
		
	}
}
