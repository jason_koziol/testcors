package com.forty2andpark.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.catalina.tribes.util.Arrays;

/**
 * Servlet implementation class Receive
 */
@WebServlet(
		description = "get strings", 
		urlPatterns = { 
				"/Receive", 
				"/receive"
		})
public class Receive extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Receive() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("receive query:" + request.getQueryString());

		HttpSession session = request.getSession(true);
		System.out.println("session: " + session.getId());

		if( session.isNew()  || null == session.getAttribute("messages") )
		{
			List<String> defaultMessages = new ArrayList<String>();
			defaultMessages.add("Hello");
			defaultMessages.add("World");
			session.setAttribute("messages", defaultMessages);
		}
		
		List<String> messages = (List<String>)session.getAttribute("messages");
		System.out.println("messages: " + messages);
		
		String message = (String)request.getParameter("message");
		if( null != message && !message.isEmpty()){
			System.out.println("message:" + message);
			messages.add(message);
		}
		else
			System.out.println("query:" + request.getQueryString());
		
		session.setAttribute("messages", messages);
		
		StringBuilder data = new StringBuilder();
		
		data.append("{ \"messages\" : [");
		for(int i = 0; i < messages.size(); i++) {
			data.append("{ \"message\":\"" + messages.get(i) + "\" }");
			if(i < messages.size() - 1)
				data.append(",");
		}
		
		data.append("]}");
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(data.toString());
		out.flush();	
	}
}
