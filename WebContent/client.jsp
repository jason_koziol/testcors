<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Client</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js" ></script>
</head>
<body>
<h2>Client</h2>
<div id="time">time</div>
<div id="messages">
	<textarea id="lines" rows="10" cols="40" readonly></textarea><br/>
	<input type="text" id="message" size="40"></input>
	<button id="send">send</button>
</div>
<script type="text/javascript">
$(document).ready(function(){
	//alert('init');
	setInterval(updateMessages,1000);

	$("#message").keydown(function(e)
	{
	    if (e.keyCode == 13)
	    {
	    	sendMessage();
	    }
	});

	$("#send").click(function(){
		sendMessage();
	});
});

function sendMessage()
{
	var message = $("#message").val();

	if( message != "" )
	{
		try{
			$.ajax({
			  url: "send?message=" + message,
			  dataType: "json",
			  success: function(data){
				  console.log("send: success");
				  $("#message").val("");
			  },
			  error: function(xhr, status, err){
				  console.log(status);
			  }
			}).done(function(data) {
			    console.log("send done. data:", data);
			  }
			);
		}
		catch(err){
			console.log("send exception - error:", err);
		}
	}
}

function updateMessages()
{
	try{
		$("#time").html(new Date().toTimeString());
		$.ajax({
		  url: "receive",
		  dataType: "json",
		  success: function(data){
			  console.log("success");
			  //$("#messages").html(data.messages);

			  var items = [];

			  $.each( data.messages, function( key, val ) {
			    items.push( val.message + "\n" );
			  });

			  items = items.slice(-10);

			  $( "#lines").html(items.join( "" ));
		  },
		  error: function(xhr, status, err){
			  console.log(status);
		  }
		}).done(function(data) {
		    console.log("done. data:", data);
		  }
		);
	}
	catch(err){
		console.log("exception - error:", err);
	}
}
</script>
</body>
</html>